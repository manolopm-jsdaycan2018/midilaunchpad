import React from 'react'
import ReactDOM from 'react-dom'
import Launchpad from './components/launchpad.js'

ReactDOM.render(<Launchpad />, document.getElementById('content'))
