# Midi launchpad

Este es un proyecto de juguete que preparé para la JSDayCAN2018

Usa webmidi y react para interactuar con un Launchpad pro.

La idea es que picando en los botones en la web se refleje a modo de "pixeles físicos" en el launchpad.

Para probarlo está en linea aquí: https://www.graph-ic.org/react-experiments/midi-demos/midilaunchpad/public/

Para usarlo en local:
- npm run build